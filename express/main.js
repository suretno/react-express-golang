const express = require("express");
const multer = require("multer");
const cors = require("cors");
const request = require("request");
const fs = require("fs");
const app = express();

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "files/");
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});

const upload = multer({
  storage: storage
}).single("file");

const uploadToGo = (filename, path) => {
  const formData = {
    name: filename,
    file: {
      value: fs.createReadStream(path),
      options: {
        filename: filename,
        contentType: "image/jpeg"
      }
    }
  };

  request.post(
    { url: "http://localhost:9000/process", formData: formData },
    function cb(err, httpResponse, body) {
      if (err) {
        return console.error("upload failed:", err);
      }
      console.log("Upload successful!  Server responded with:", body);
    }
  );
};

app.use(cors());

app.get("/", (req, res) => {
  res.send("Express Server");
});

app.post("/upload", (req, res) => {
  upload(req, res, err => {
    if (err instanceof multer.MulterError) {
      return res.status(500).json(err);
    } else if (err) {
      return res.status(500).json(err);
    }

    return setTimeout(() => {
      const { filename, path } = req.file;

      uploadToGo(filename, path);

      return res.status(200).send(req.file);
    }, 3000);
  });
});

app.listen(8000);

console.log("server started at http://localhost:8000");
